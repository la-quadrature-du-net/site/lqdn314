<?php
class blackout
{
	static $blackout_code_name = "Pink Out";
	static $esquive_param = 'Imwhashingcat';
	static $english_file = 'pinkout_en.html';
	static $french_file = 'pinkout_fr.html';
	static $bgcolor = '#FFABCE';
	static $fgcolor = '#000';
	static $langs = array('fr'); // liste des langues à blackouter
	static $oneTime = true;

	function header()
	{
		$html = '<div id="blackout_cache" style="z-index:1000000;position:fixed;top:0;width:80%;height:100%;text-align:center;padding:5% 10%;background-color:'.self::$bgcolor.';color:'.self::$fgcolor.'">';
		return $html;
	}
	function footer()
	{
		global $language;
		return "</div>";
		$html = '<p style="font-weight:bold;font-size:1.3em;text-align:center"><a style="padding:20px;display:block" href="?'.self::$esquive_param.'=1">';
		$link_esquive = 'Click here to access requested page.';
		if($language->language == 'fr') 
		{
			$link_esquive = 'Cliquer ici pour accéder à la page demandée.';
		}
		$html .= $link_esquive.'</a></p></div>';
		
		return $html;
	}
	function BoHttpHeader()
	{
		if(headers_sent()) return false;
		header('HTTP/1.1 503 '.self::$blackout_code_name);
	}
	function isBlackOutable()
	{
		global $language;
		if (!in_array($language->language,self::$langs)) return false;
		if (!empty($_REQUEST[self::$esquive_param])) return false;
		if (!empty($_SESSION[self::$esquive_param])) return false;	
//		if (substr($_SERVER["REQUEST_URI"],strpos($_SERVER["REQUEST_URI"],'?')-5,5) == "/acta")	return false;
		if ($logged_in) return false;
		if (!empty($_POST)) return false;
		return true;
	}
	function isEsquive()
	{
		if (empty($_REQUEST[self::$esquive_param])) return false;
		if (self::$oneTime)
		{
			$_SESSION[self::$esquive_param] = 1;
		}
		return true;
	}
	function activate()
	{
		if (self::isEsquive()) return false;
        	if (!self::isBlackOutable()) return false;
		self::BoHttpHeader();
		self::show();
		return true;
	}
	function show()
	{
		global $language;
		$file = self::$english_file;
		if ($language->language=='fr') $file = self::$french_file;
		$file = getcwd().'/'.$file;
		if (!file_exists($file)) return false;
		echo self::header();
		readfile($file);
		echo self::footer();
	}
}
?>

<!DOCTYPE html>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
  <head>
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <?php print $styles; ?>
    <?php print $head_scripts; ?>
    <?php print $scripts; ?>

<style type="text/css">
.pipink {
background-color:#FFABCE;
color:#540023;
text-align:justify;
}
.pipink a:link, .pipink a:visited, .pipink a:active {
color:#a80046;
} 
</style>

  </head>

<?php
if (!empty($sidebar))
{
    $body_classes = str_replace('no-sidebars', '', $body_classes);
}
?>

  <body role="document" class="<?php print $body_classes; ?>">
  <?php /* $blackout = blackout::activate(); if (!$blackout && !empty($_GET['force_pink']))blackout::show();*/ ?>
    <div id="page">

      <div id="top">
        <?php if ($top_navigation) print $top_navigation; ?>
        <?php if ($lang_switcher): ?>
        <div id="lang-switcher">
          <?php print $lang_switcher; ?>
        </div>
        <?php endif; ?>
        <?php if ($search_box) print $search_box; ?>
      </div><!-- /top -->

      <div id="header" role="banner" class="clearfix">
        <div id="header-left">
          <?php if (!empty($logo)): ?>
          <div id="logo">
            <a rel="home" title="<?php print t('Home'); ?>" href="<?php print check_url($front_page); ?>">
              <img src="<?php print check_url($logo); ?>" alt="<?php print $site_name; ?>" title="<?php print $site_name; ?>" />
            </a>
          </div>
          <?php endif; ?>
          <h1 id="site-name">
            <a rel="home" title="<?php print t('Home'); ?>" href="<?php print check_url($front_page); ?>">
              <?php print $site_name; ?>
            </a>
          </h1>
          <div id="site-slogan"><?php print $site_slogan?$site_slogan:'Internet & Libertés'; ?></div>
        </div>
        <div id="header-right">
          <?php if ($header_navigation) print $header_navigation; ?>
          <?php if ($share_links) print $share_links; ?>
          <?php if (!empty($header)): ?>
          <div id="header-region">
            <?php print $header; ?>
          </div>
          <?php endif; ?>
        </div>
        <div id="navigation" role="navigation" class="clearfix">
          <?php if (!empty($dossiers_menu)): ?>
            <h3><?php print $dossiers_menu['subject']; ?></h3>
            <div class="dossiers-container"><?php print $dossiers_menu['content']; ?></div>
          <?php endif; ?>
        </div>
      </div><!-- /header -->

      <div id="container" class="clearfix">
        <div id="content" role="main" class="<?php if(!empty($sidebar)) print 'with-sidebar'; ?>">
          <?php
          //Par snip, le 6 octobre 2009
          $node = (trim(substr($content, 0, 14))=='<div id="node-'); //début du node.tpl.php
          if ($node):
          ?>
<?php if($language->language == 'fr')  { ?>
  	     <div style="text-align: center; padding-bottom: 20px"><a href="https://soutien.laquadrature.net" title="Soutenons La Quadrature du Net !"><img src="https://www.laquadrature.net/files/banniere_pixel.gif" alt="Soutenons La Quadrature du Net !" /></a></div>
<?php } else { ?>
             <div style="text-align: center; padding-bottom: 20px"><a href="https://support.laquadrature.net" title="Support La Quadrature du Net!"><img src="https://www.laquadrature.net/files/banniere_pixel_en.gif" alt="Support La Quadrature du Net!" /></a></div> 

<?php } ?>
	  <?php endif; ?>
          <?php if (!empty($title)): ?><h1 class="title" id="page-title"><?php print t($title); ?></h1><?php endif; ?>
          <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
          <?php if (!empty($messages)): print $messages; endif; ?>
          <?php if (!empty($help)): print $help; endif; ?>
          <div id="content-content">
		    <?php print $content; ?>
          </div><!-- /content-content -->
          <?php print $feed_icons; ?>


	    <?php  if($language->language == 'fr')  { 
	  // choisis une url au hasard dans ce tas : 
$burls=array(
	     "https://soutien.laquadrature.net/images/support_lqdn_468_A.png",
	     "https://soutien.laquadrature.net/images/support_lqdn_468_B.png",
	     "https://soutien.laquadrature.net/images/support_lqdn_468_C.png",
	     );
$burl=$burls[rand(0,count($burls)-1)];
?>
	  <div style="text-align: center; padding-bottom: 20px"><a href="https://soutien.laquadrature.net/" title="Soutenez La Quadrature du Net!" alt="Soutenez La Quadrature du Net!"><img src="<?php echo $burl; ?>" alt="Soutenez La Quadrature du Net!" /></a></div>
	     <?php } else { 
$burls=array(
	     "https://support.laquadrature.net/images/support_lqdn_468_A_EN.png",
	     "https://support.laquadrature.net/images/support_lqdn_468_B_EN.png",
	     "https://support.laquadrature.net/images/support_lqdn_468_C_EN.png",
             );
$burl=$burls[rand(0,count($burls)-1)];
?>

	  <div style="text-align: center; padding-bottom: 20px"><a href="https://support.laquadrature.net/" title="Support La Quadrature du Net!" alt="Support La Quadrature du Net!"><img src="<?php echo $burl; ?>" alt="Support La Quadrature du Net!" /></a></div>


	   <?php } ?>


        </div><!-- /content -->
	    <?php if (!empty($sidebar)): ?>
        <div id="sidebar">
          <?php print $sidebar; ?>
        </div><!-- /sidebar -->
	    <?php endif; ?>
      </div><!-- /container -->

	  <div id="footer" role="contentinfo" class="clearfix">
        <?php if (!empty($footer)): print $footer; endif; ?>
        <?php print $lqdn_footer_message; ?>
	  </div><!-- /footer -->
      <div id="bottom">
	    <?php print $bottom; ?>
      </div>

    </div><!-- /page -->

<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function() {
  var u=(("https:" == document.location.protocol) ? "https" : "http") + "://piwik.lqdn.fr/";
  _paq.push(['setTrackerUrl', u+'piwik.php']);
  _paq.push(['setSiteId', 3]);
  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
  g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
})();
</script>
<noscript><p><img src="https://piwik.lqdn.fr/piwik.php?idsite=3" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->

    <?php print $closure; ?>
  </body>
</html>
